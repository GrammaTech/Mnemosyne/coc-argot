.PHONY: clean ts

version := $(shell jq -r '.version' package.json)

all: ts

json_files := $(wildcard *.json)
ts_files := $(wildcard src/*.ts)

ts: $(ts_files) $(json_files)
	npm run compile

clean:
	rm -f *.vsix
	rm -rf out/*
