import * as vscode from 'coc.nvim';
import * as child_process from 'child_process';
import * as net from 'net';
import * as util from 'util';
import {
    workspace, Disposable, ExtensionContext, languages,
    window, commands
} from 'coc.nvim';
import {
    LanguageClient, LanguageClientOptions, ServerOptions,
    TextDocumentIdentifier
} from 'coc.nvim';

import {promises as fs} from 'fs';
import * as url from 'url';
import * as path from 'path';

let languageClient: LanguageClient;

function retry<T>(retries: number, fn: () => Promise<T>): Promise<T> {
    return fn().catch((err) => retries > 1 ? retry(retries - 1, fn) : Promise.reject(err));
}
const pause = (duration: number) => new Promise(res => setTimeout(res, duration));
function backoff<T>(retries: number, fn: () => Promise<T>, delay = 500): Promise<T> {
    return fn().catch(err => retries > 1
        ? pause(delay).then(() => backoff(retries - 1, fn, delay * 2))
        : Promise.reject(err));
}

let ctx: ExtensionContext;

const defaultExcludes = [
    /^\.(?:git|svn|hg|bzr|pytest_cache|DS_Store|nyc_output)$/,
    // Exclude LICENSE for consistency with VS Code.
    /^(?:node_modules|bower_components|TAGS|__pycache__|LICENSE)$/,
    /~$/,
    /\.min\./
];

function pathParts(file: string) {
    const p = path.parse(file);
    const parts = p.dir.split(path.sep).concat([p.base]);
    return parts;
}

async function listFilesInDir (base: string, excludes = defaultExcludes): Promise<string[]|undefined> {
    // https://stackoverflow.com/questions/5827612/node-js-fs-readdir-recursive-directory-search
    async function* getFiles (dir: string): AsyncIterable<string> {
        const dirents = await fs.readdir(dir, { withFileTypes: true });
        for (const dirent of dirents) {
            const res = path.resolve(dir, dirent.name);
            if (dirent.isDirectory()) {
                yield* getFiles(res);
            } else {
                const parts = pathParts(res);
                // XXX Quadratic.
                const excluded = excludes.find(function (exclude) {
                    return parts.find(function (part) {
                        return new RegExp(exclude).test(part);
                    });
                });
                if (!excluded) {
                    yield res;
                }
            }
        }
    }

    const result: string[] = [];

    try {
        for await (let file of getFiles(base)) {
            result.push(file);
        }
    } catch (Exception) {
        return undefined;
    }
  return result;
}

function mnemosyneConnect () {
    const context = ctx;
    const serverOptions: ServerOptions = async () => {
        const client = new net.Socket();
        return await backoff(5, () => {
            return new Promise((resolve, reject) => {
                const port = workspace.getConfiguration().get<number>('mnemosyne.server.port');
                const host = workspace.getConfiguration().get<string>('mnemosyne.server.host');
                setTimeout(() => {
                    reject(new Error("Connection to Mnemosyne times out"));
                }, 1000);
                client.connect(port!, host!);
                client.once('connect', () => {
                    resolve({
                        reader: client,
                        writer: client,
                    });
                });
            });
        });
    };

    const clientOptions: LanguageClientOptions = {
        documentSelector: ['commonlisp', 'python', 'c', 'cpp',
                           { scheme: 'file', language: 'javascript'},
                           { scheme: 'file', language: 'typescript'},
                           { scheme: 'file', language: 'typescriptreact'}
                          ],
        // TODO Can't make this work without incompatible library versions.
        // revealOutputChannelOn: "never",
        initializationOptions: {
            argot: {
                disable: (workspace.getConfiguration().get<string>('mnemosyne.server.disable') || "")
                             .split(',')
                             .map(x => x.trim()),
                filesProvider: true,
                contentProvider: true,
                inputBoxProvider: true
            }
        }
    };

    languageClient = new LanguageClient(
        "mnemosyne", // ID for configuration
        "Mnemosyne", // Name for display
        serverOptions,
        clientOptions
    );

    languageClient.onReady().then(function (_x: any) {
        languageClient.onRequest("argot/window/showPrompt", async (params: any) => {
            const title = await window.requestInput(params.message, params.defaultValue);
            return {
                "title": title
            };
        });
        languageClient.onRequest("argot/textDocument/content", async (params: any) => {
            const doc = params.textDocument;
            let p = url.fileURLToPath(new url.URL(doc.uri));
            if (vscode.workspace.rootPath) {
                p = path.resolve(vscode.workspace.rootPath, p);
            }
            // TODO This will break for unusual encodings. Also: is a
            // race condition possible here, where the file has been
            // opened and edited on the VS Code side but no didOpen
            // notification has been sent?
            const buf = await fs.readFile(p);
            if (buf.includes('\0')) {
                throw new Error('Binary file!');
            }
            const text = buf.toString('utf-8');
            return {...doc, languageId: '', version: 0, text: text}
        })
        languageClient.onRequest("argot/workspace/files", async (params: any): Promise<TextDocumentIdentifier[]> => {

            const base = params.base
                ? path.resolve(vscode.workspace.rootPath, params.base)
                : vscode.workspace.rootPath;

            const files = await listFilesInDir(base);

            if (!files) {
                return [];
            } else {
                return files.map(p => ({ uri: url.pathToFileURL(p).toString()} ));
            }
        });
    });

    context.subscriptions.push(languageClient.start());
}

export async function activate(context: ExtensionContext) {
    ctx = context;
    context.subscriptions.push(commands.registerCommand("mnemosyne.connect", mnemosyneConnect));
}

export function deactivate() {
}
