# Mnemosyne Vim extension

Extension for using Mnemosyne with Vim/Neovim via [Conqueror of Completion][COC].

This is forked from our [VS Code extension][vscode].

[coc]: https://github.com/neoclide/coc.nvim
[vscode]: https://gitlab.com/GrammaTech/Mnemosyne/mnemosyne-vscode
